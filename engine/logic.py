from engine.alphabet import Alphabet


class Action:
    def __init__(self, state=None, character=None, movement=None):
        self.state = state
        self.character = character
        self.movement = movement


class Panic(Action):
    def __init__(self):
        super(Panic, self).__init__(state=-1)


class Logic:
    def __init__(self, alphabet: Alphabet, states=1):
        self.alphabet = alphabet
        self.states = states
        self.logic = []
        for i in range(self.states):
            self.logic.append({})
            for j in self.alphabet.body:
                self.logic[-1][j] = Panic()

    def join(self, logic):
        self.alphabet = logic.alphabet
        self.states = logic.states
        self.logic = logic.logic

    def set_alphabet(self, alphabet: Alphabet):
        for i in self.alphabet.body:
            if i not in alphabet.body:
                for j in self.logic:
                    j.pop(i)
        for i in alphabet.body:
            if i not in self.alphabet.body:
                for j in self.logic:
                    j[i] = Panic()

        self.alphabet = alphabet

    def set_count(self, count: int):
        if self.states < count:
            for i in range(count - self.states):
                self.logic.append({})
                for j in self.alphabet.body:
                    self.logic[-1][j] = Panic()
        if self.states > count:
            for i in range(self.states - count):
                self.logic.pop()

        self.states = count
