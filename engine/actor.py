from engine.alphabet import Alphabet
from engine.logic import Logic, Action
from engine.tape import Tape


class Actor:
    def __init__(self, state_count=1, state=0, logic=None, tape=None, alphabet=None, length=20, position=0):
        self.panic = 0
        self.state_count = state_count
        self.state = state
        self.length = length
        self.position = position
        if alphabet is None:
            alphabet = Alphabet('_1')
        self.alphabet = alphabet
        if logic is None:
            logic = Logic(self.alphabet, self.state_count)
        self.logic = logic
        if tape is None:
            tape = Tape(self.length, self.alphabet.empty())
        self.tape = tape

    def set_position(self, position: int):
        if self.panic:
            return
        if 0 <= position < self.length:
            self.position = position
        else:
            self.panic = 2

    def left(self):
        self.set_position(self.position - 1)

    def right(self):
        self.set_position(self.position + 1)

    def set_state(self, state: int):
        if self.panic:
            return
        if 0 <= state < self.state_count:
            self.state = state
        else:
            self.panic = 4

    def set_character(self, char: str):
        if self.panic:
            return
        if char in self.alphabet.body:
            self.tape.set(char, self.position)
        else:
            self.panic = 3

    def step(self):
        instruction: Action
        instruction = self.logic.logic[self.state][self.tape.get(self.position)]
        if instruction.state is not None:
            self.set_state(instruction.state)
        if instruction.character is not None:
            self.set_character(instruction.character)
        if instruction.movement is not None:
            {'<': self.left, '>': self.right}[instruction.movement]()


def empty():
    pass


if __name__ == '__main__':
    a = Alphabet('_1')
    test_logic = Logic(a, 2)
    test_logic.logic[0]['_'] = Action(1, '1', '<')
    c = Actor(state_count=2, alphabet=a, logic=test_logic)
    print(c.tape.body, c.state, c.position, c.panic)
    c.step()
    print(c.tape.body, c.state, c.position, c.panic)
