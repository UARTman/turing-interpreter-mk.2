class Alphabet:
    def __init__(self, string: str):
        self.body = str2a(string)

    def empty(self):
        return self.body[0]

    def update(self, string: str):
        self.body = str2a(string)


def str2a(string):
    string1 = ''
    for i in string:
        if i not in string1:
            string1 += i
    return string1
