from engine.alphabet import Alphabet
from engine.logic import Logic, Action
from engine.tape import Tape
from legacy.fileops import get_logic


def f2a(f):
    return Alphabet(f.read().replace('\n', ''))


def a2f(f, a: Alphabet):
    f.write(a.body)


def fn2a(fn):
    with open(fn, 'r') as f:
        return f2a(f)


def a2fn(fn, a: Alphabet):
    with open(fn, 'w') as f:
        a2f(f, a)


def fn2l(fn: str, l: Logic):
    a = get_logic(fn)
    b = Logic(l.alphabet, a[0][1])
    b.set_count(a[0][1])
    print(len(b.logic))
    for i in a[1]:
        b.logic[i[0]][i[1]] = Action(state=a[1][i][0], character=a[1][i][1],
                                     movement={0: None, 1: '>', -1: '<'}[a[1][i][2]])
        print(i)
    print(len(b.logic))
    l.set_count(a[0][1])
    l.join(b)
    l.set_count(a[0][1])
    return l


def fn2t(fn: str, tape: Tape):
    with open(fn, 'r') as f:
        ii = 0
        for i in f.read().replace('\n',''):
            tape.body[ii] = i
            ii += 1
    return tape