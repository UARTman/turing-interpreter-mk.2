class Tape:
    def __init__(self, length: int, empty='_'):
        self.length = length
        self.body = []
        for i in range(length):
            self.body.append(empty)

    def set(self, s: str, i: int):
        self.body[i] = s

    def get(self, i: int):
        return self.body[i]
