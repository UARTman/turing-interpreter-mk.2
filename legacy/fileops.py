def get_logic(fn):
    ret = {}
    with open(fn, 'r') as f:
        b = f.read()
    b = b.split("\n#instructions\n")
    c = b[0].split("/")
    d = b[1].split("\n")
    d = list(map(a_split, d))
    for i in d:
        if i[0] is None:
            continue
        ret[i[0]] = i[1]
    return (int(c[0]), int(c[1])), ret

def a_split(s):
    c = s.split("do")
    return list(map(b_split, c))

def b_split(s):
    s = s.split()
    if len(s) == 2:
        return int(s[0]), s[1]
    if len(s) == 3:
        return int(s[0]), s[1], int(s[2])
