from flexx import flx

from engine.actor import Actor
from engine.alphabet import Alphabet
from engine.file_ops import *


class UserInput(flx.PyWidget):
    text: flx.LineEdit
    button: flx.Button
    tape: list
    logic: list
    file_browser: flx.FileBrowserWidget
    file_layout: flx.VBox
    layout: flx.PinboardLayout
    file_select_button: flx.Button
    file_close_button: flx.Button
    file_choose_button: flx.Button
    actor: Actor
    state_options: tuple
    character_options: tuple
    movement_options: tuple
    left: flx.Button
    right: flx.Button
    tapebox: flx.HBox
    logicbox: flx.HBox

    def init(self):
        self.actor = Actor()
        self.movement_options = ('None', '>', '<')
        a = list(map(str, range(self.actor.state_count + 1)))
        a.append('None')
        a.append('-1')
        self.state_options = tuple(a)
        a = list(self.actor.alphabet.body)
        a.append('None')
        self.character_options = tuple(a)
        with flx.PinboardLayout() as self.layout:
            with flx.VBox():
                with flx.HBox() as self.tapebox:  # Tape Indicator
                    self.tape = []
                    for i in range(self.actor.length):
                        color = "black"
                        if i == self.actor.position:
                            color = "red"
                        self.tape.append(
                            flx.ComboBox(options=tuple(list(self.actor.alphabet.body)),
                                         text=self.actor.alphabet.empty(),
                                         style="color:" + color))
                    flx.Widget(flex=1)
                    self.panic = flx.Label(text="Panic " + str(self.actor.panic))

                with flx.HBox():
                    with flx.VBox():  # Alphabet + logic
                        with flx.HBox():
                            self.alphabet = flx.LineEdit(text=self.actor.alphabet.body, flex=1)  # Alphabet Field
                            self.count = flx.LineEdit(text=str(self.actor.state_count), style="width:5px")
                        with flx.HBox() as self.logicparent:  # Logic Table
                            with flx.HBox(flex=1) as self.logicbox:
                                self.alogic = []
                                pass
                        flx.Widget(flex=1)  #
                    flx.Widget(flex=1)

                    with flx.VBox():  # Button Controls
                        self.step = flx.Button(text="Step")
                        with flx.HBox():
                            self.left = flx.Button(text="<", flex=1)
                            self.right = flx.Button(text=">", flex=1)
                        self.reset = flx.Button(text="Reset")
                        with flx.HBox():
                            with flx.VBox():
                                flx.Label(text='Operation')
                                self.radio_save = flx.RadioButton(text='Save')
                                self.radio_load = flx.RadioButton(text='Load')
                                flx.Widget(flex=1)
                            with flx.VBox():
                                flx.Label(text='Target')
                                self.radio_alphabet = flx.RadioButton(text='Alphabet')
                                self.radio_logic = flx.RadioButton(text='Logic')
                                self.radio_tape = flx.RadioButton(text='Tape')
                                flx.Widget(flex=1)
                        with flx.HBox():
                            flx.Label(text="File: ", flex=1, style="width:100px")
                            self.file_choose_button = flx.Button(text="Change")
                        self.fop = flx.Button(text="File Operation")
                        flx.Widget(flex=1)
                flx.Widget(flex=1)

        with self.layout:  # File Dialog
            with flx.VBox(
                    style='background-color:white;width:450px;height:450px;visibility:hidden') as self.file_layout:
                with flx.HBox():
                    flx.Label(flex=1, text="File Chooser")
                    self.file_close_button = flx.Button(text='X')
                self.file_browser = flx.FileBrowserWidget(path='.', flex=1)
                with flx.HBox():
                    flx.LineEdit(flex=1)
                    self.file_select_button = flx.Button(text='Select')

        self.rebuild_logic()
        self.update()

    @flx.action
    def show_filedialog(self):
        self.file_layout.apply_style("background-color:white;width:450px;height:450px;visibility:visible")

    @flx.action
    def hide_filedialog(self):
        self.file_layout.apply_style('background-color:white;width:450px;height:450px;visibility:hidden')

    @flx.reaction("file_close_button.pointer_click")
    def close_filedialog(self, *events):
        self.hide_filedialog()

    @flx.reaction("file_choose_button.pointer_click")
    def open_filedialog(self, *events):
        self.show_filedialog()

    @flx.reaction("left.pointer_click")
    def go_left(self, *events):
        self.actor.left()
        self.update()

    @flx.reaction("right.pointer_click")
    def go_right(self, *events):
        self.actor.right()
        self.update()

    @flx.reaction("step.pointer_click")
    def do_step(self, *events):
        self.actor.step()
        self.update()

    @flx.reaction("reset.pointer_click")
    def do_reset(self, *events):
        self.actor.panic = 0
        self.actor.position = 0
        self.actor.state = 0
        self.update()

    @flx.reaction("tape*.text")
    def catch_tape(self, *events):
        for i in range(len(self.tape)):
            self.actor.tape.body[i] = self.tape[i].text
        self.update()

    @flx.reaction("alphabet.user_done")
    def catch_alphabet(self, *events):
        self.actor.alphabet = Alphabet(self.alphabet.text)
        self.actor.logic.set_alphabet(self.actor.alphabet)
        print(self.actor.logic.logic)
        self.rebuild_logic()
        self.update()

    @flx.reaction("fop.pointer_click")
    def openfile(self, *events):
        self.actor.alphabet = fn2a("files/alphabet.txt")
        self.actor.logic.set_alphabet(self.actor.alphabet)
        self.actor.logic = fn2l("files/logic.txt", self.actor.logic)
        self.actor.tape = fn2t("files/tape.txt", self.actor.tape)
        self.actor.state_count = self.actor.logic.states
        self.rebuild_logic()
        self.update()

    #@flx.reaction("alogic*.user_selected")
    def catch_logic(self, *events):
        for i in range(self.actor.state_count):
            for j in self.actor.alphabet.body:
                state = self.logic[i][j][0].text
                if state == 'None':
                    state = None
                    self.actor.logic.logic[i][j].state = state
                else:
                    self.actor.logic.logic[i][j].state = int(state)
                character = self.logic[i][j][1].text
                if character == 'None':
                    character = None
                movement = self.logic[i][j][2].text
                if movement == 'None':
                    movement = None
                self.actor.logic.logic[i][j].character = character
                self.actor.logic.logic[i][j].movement = movement
                print(self.logic[i][j][0].selected_index, self.logic[i][j][1].selected_index,
                      self.logic[i][j][2].selected_index, )
                print(state, character, movement)
                print(i, j, self.actor.logic.logic[i][j].state, self.actor.logic.logic[i][j].character,
                      self.actor.logic.logic[i][j].movement)
        self.update()

    @flx.reaction("count.user_done")
    def catch_count(self, *events):
        self.actor.state_count = int(self.count.text)
        self.actor.logic.set_count(self.actor.state_count)
        self.rebuild_logic()
        self.update()

    @flx.action
    def rebuild_logic(self):
        print(self.actor.logic.logic)
        a = list(map(str, range(self.actor.state_count + 1)))
        a.append('None')
        a.append('-1')
        self.state_options = tuple(a)
        a = list(self.actor.alphabet.body)
        a.append('None')
        self.character_options = tuple(a)
        self.logicbox.dispose()
        with self.logicparent:  # Logic Table
            with flx.HBox(flex=1) as self.logicbox:
                self.alogic = []
                self.states = []
                with flx.VBox():
                    flx.Label(text='-')
                    for i in self.actor.alphabet.body:
                        flx.Label(text=i)
                self.logic = []
                for i in range(self.actor.state_count):
                    self.logic.append({})
                    with flx.VBox():
                        with flx.HBox():
                            flx.Widget(flex=1)
                            self.states.append(flx.Label(text=str(i)))
                            flx.Widget(flex=1)
                        for j in self.actor.alphabet.body:
                            self.logic[-1][j] = []
                            with flx.HBox():
                                self.logic[-1][j].append(
                                    ComboInputP(i, j, 0, self.actor, text=str(self.actor.logic.logic[i][j].state),
                                                 options=self.state_options))
                                self.alogic.append(self.logic[-1][j][-1])
                                self.logic[-1][j].append(
                                    ComboInputP(i, j, 1, self.actor, text=str(self.actor.logic.logic[i][j].character),
                                                 options=self.character_options))
                                self.alogic.append(self.logic[-1][j][-1])
                                self.logic[-1][j].append(
                                    ComboInputP(i, j, 2, self.actor, text=str(self.actor.logic.logic[i][j].movement),
                                                 options=self.movement_options))
                                self.alogic.append(self.logic[-1][j][-1])
        for i in self.tape:
            i.set_options(tuple(list(self.actor.alphabet.body)))

    @flx.action
    def update(self):
        self.panic.set_text("Panic " + str(self.actor.panic))
        for i in range(len(self.tape)):
            self.tape[i].set_text(self.actor.tape.body[i])
            color = "black"
            if i == self.actor.position:
                color = "red"
            self.tape[i].apply_style("color:" + color)
        for i in range(self.actor.state_count):
            for j in self.actor.alphabet.body:
                self.logic[i][j][0].ci.set_text(str(self.actor.logic.logic[i][j].state))
                self.logic[i][j][1].ci.set_text(str(self.actor.logic.logic[i][j].character))
                self.logic[i][j][2].ci.set_text(str(self.actor.logic.logic[i][j].movement))
        self.alphabet.set_text(self.actor.alphabet.body)
        self.count.set_text(str(self.actor.state_count))
        for i in range(len(self.states)):
            if i == self.actor.state:
                self.states[i].apply_style("color:red")
            else:
                self.states[i].apply_style("color:black")


class ComboInputP(flx.PyWidget):
    ci: flx.ComboBox

    def __init__(self, i, j, box_type, actor, **kwargs):
        super(ComboInputP, self).__init__()
        self.ci = flx.ComboBox(**kwargs)
        self.i = i
        self.j = j
        self.type = box_type
        self.actor = actor

    def init(self):
        pass

    @flx.reaction
    def selection(self, *events):
        print(self.i, self.j, self.ci.text)
        text = self.ci.text
        if self.type == 0:
            state = text
            if state == 'None':
                self.actor.logic.logic[self.i][self.j].state = None
            else:
                self.actor.logic.logic[self.i][self.j].state = int(state)
        if self.type == 1:
            character = text
            if character == 'None':
                character = None
            self.actor.logic.logic[self.i][self.j].character = character
        if self.type == 2:
            movement = text
            if movement == 'None':
                movement = None
            self.actor.logic.logic[self.i][self.j].movement = movement


if __name__ == '__main__':
    mainApp = flx.App(UserInput)
    mainApp.launch('chrome-app')
    flx.run()